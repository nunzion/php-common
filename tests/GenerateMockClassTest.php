<?php

namespace Nunzion\CodeEmit;

use Nunzion\Types\Type;
use PHPUnit_Framework_TestCase;
use stdClass;

class GenerateMockClassTest extends PHPUnit_Framework_TestCase
{
    public function testFoo()
    {
        $mockupBuilder = new MockupBuilder();
        $result = $mockupBuilder->generateMockup(Type::of("Nunzion\CodeEmit\TestInterface"));
        
        $callReceiver = new MockupCallLogger();
        $m = $result->createMockup($callReceiver);
        
        print_r($result->getMockupCode());
        
        $this->assertTrue($m instanceof TestInterface);
        $m->bla((object)array("test" => "foo"));
        
    }
}

interface TestInterface
{
    function test($x);
    function bla(stdClass $x, $foo = array('a'));
}

abstract class TestClass
{
    public abstract function foo($args);
    
}