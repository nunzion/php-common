<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion;


class StringHelperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers Nunzion\StringHelper::startsWith
     */
    public function testStartsWith()
    {
        $this->assertTrue(StringHelper::startsWith("foo", "f"));
        $this->assertTrue(StringHelper::startsWith("bar", "bar"));
        
        $this->assertFalse(StringHelper::startsWith("bar", "baz"));
        $this->assertFalse(StringHelper::startsWith("bar", "ar"));
        $this->assertFalse(StringHelper::startsWith("bar", "a"));
    }

    /**
     * @covers Nunzion\StringHelper::endsWith
     */
    public function testEndsWith()
    {
        $this->assertTrue(StringHelper::endsWith("foo", "oo"));
        $this->assertTrue(StringHelper::endsWith("bar", "bar"));
        
        $this->assertFalse(StringHelper::endsWith("bar", "baz"));
        $this->assertFalse(StringHelper::endsWith("bar", "ba"));
        $this->assertFalse(StringHelper::endsWith("bar", "a"));
    }

    /**
     * @covers Nunzion\StringHelper::contains
     */
    public function testContains()
    {
        $this->assertTrue(StringHelper::contains("foo", "oo"));
        $this->assertTrue(StringHelper::contains("bar", "bar"));
        $this->assertTrue(StringHelper::contains("string", "tri"));
        $this->assertTrue(StringHelper::contains("text", "t"));
        
        $this->assertFalse(StringHelper::contains("bar", "baz"));
        $this->assertFalse(StringHelper::contains("bar", "ba "));
        $this->assertFalse(StringHelper::contains("bar", "bart"));
    }

    /**
     * @covers Nunzion\StringHelper::format
     */
    public function testFormat()
    {
        $this->assertEquals("Hello World, my name is John Doe",
            StringHelper::format("Hello {0}, {1} is {2}", 
                "World", "my name", "John Doe"));
        
        $this->assertEquals("Hello World, my name is John Doe",
            StringHelper::format("Hello {0}, {1} is {2}", 
                array("World", "my name", "John Doe")));
        
        $this->assertEquals("Hello World, my name is John Doe",
            StringHelper::format("Hello {name}, {valueName} is {value}", 
                array("name" => "World", "valueName" => "my name", "value" => "John Doe")));
        
        $this->assertEquals("Test Test Test",
            StringHelper::format("{value} {value} {value}", 
                array("value" => "Test")));
        
        $this->assertEquals("Tody is 01.01.2000 00:00:00",
            StringHelper::format("Tody is {0:d.m.Y H:i:s}", 
                new \DateTime('2000-01-01')));
        
        $this->assertEquals("Test {value} Test",
            StringHelper::format("{value} {{value}} {value}", 
                array("value" => "Test")));
    }

}
