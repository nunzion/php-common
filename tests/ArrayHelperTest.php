<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion;


class ArrayHelperTest extends \PHPUnit_Framework_TestCase
{
    public function testToString()
    {
        $this->assertEquals("1, 2, 3, 4", ArrayHelper::toString(array(1, 2, 3, 4), ", "));
    }
}
