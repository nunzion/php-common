<?php

namespace Nunzion\CodeEmit;

use Nunzion\CodeEmit\SyntaxTree\ClassDefinition;

class MockupBuilderResult
{
    private $mockupClass;
    private static $id = 0;
    private $classCode;
    private $className;
    
    public function __construct(SyntaxTree\ClassDefinition $mockupClass)
    {
        $this->mockupClass = $mockupClass;
    }
    
    public function getMockupCode()
    {
        if ($this->classCode === null)
        {
            $id = self::$id;
            self::$id = $id + 1;
            
            $this->mockupClass->setFullName($this->mockupClass->getFullName() . $id);
            
            $emitter = new CodeEmitter();
            $this->classCode = $emitter->emit($this->mockupClass);
        }
        
        return $this->classCode;
    }
    
    public function createMockup(MockupCallReceiver $receiver)
    {
        if ($this->className === null)
        {
            $code = $this->getMockupCode();
            eval($code);
            $this->className = $this->mockupClass->getFullName();
        }
        
        $cn = $this->className;
        
        return new $cn($receiver);
    }
}
