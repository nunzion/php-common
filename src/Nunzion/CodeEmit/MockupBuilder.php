<?php

namespace Nunzion\CodeEmit;

use Nunzion\CodeEmit\SyntaxTree\Assignment;
use Nunzion\CodeEmit\SyntaxTree\ClassDefinition;
use Nunzion\CodeEmit\SyntaxTree\Constructor;
use Nunzion\CodeEmit\SyntaxTree\FieldRef;
use Nunzion\CodeEmit\SyntaxTree\MemberVisibility;
use Nunzion\CodeEmit\SyntaxTree\Method;
use Nunzion\CodeEmit\SyntaxTree\Parameter;
use Nunzion\CodeEmit\SyntaxTree\ThisRef;
use Nunzion\CodeEmit\SyntaxTree\VariableRef;
use Nunzion\Expect;
use Nunzion\Types\Type;
use ReflectionMethod;
use ReflectionParameter;

class MockupBuilder
{
    /**
     * 
     * @param Type $typeToMock
     * @return MockupBuilderResult
     */
    public function generateMockup(Type $typeToMock)
    {
        if (!($typeToMock->isClass() || $typeToMock->isInterface()))
            Expect::that($typeToMock)->_("is either interface or class");
                
        $r = $typeToMock->getReflectionClass();

        $mockupClass = new ClassDefinition($typeToMock->getName() . "_Nunzion_MockupBuilder_MockImpl");
        
        if ($typeToMock->isInterface())
            $mockupClass->addImplementedInterface($typeToMock->getName());
        else if ($typeToMock->isClass())
            $mockupClass->setBaseClass($typeToMock->getName());
        
        $receiverFieldName = "__nunzion_mock_receiver";

        foreach ($r->getMethods(ReflectionMethod::IS_PUBLIC, ReflectionMethod::IS_PROTECTED, 
                ReflectionMethod::IS_ABSTRACT, ReflectionMethod::IS_FINAL) as $m)
        {
            if ($m->isConstructor())
                continue;
            
            if ($m->isStatic())
                continue;
            
            $md = new Method($m->getName());
            $md->setVisibility(MemberVisibility::getPublic());
            
            $md->setParameters($this->getParameters($m));
            $md->setCode($this->getCode($md, $receiverFieldName));
            
            $mockupClass->addMethod($md);
        }
        
        $ctor = new Constructor();
        $ctor->setVisibility(MemberVisibility::getPublic());
        
        $ctor->addParameter(new Parameter("receiver", "Nunzion\CodeEmit\MockupCallReceiver"));
        $ctor->setCode("\$this->$receiverFieldName = \$receiver;");
        $mockupClass->addMethod($ctor);
        
        return new MockupBuilderResult($mockupClass);
    }
    
    private function getCode($md, $receiverFieldName)
    {
        $array = "array(";
        $first = true;
        foreach ($md->getParameters() as $p)
        {
            if ($first)
                $first = false;
            else
                $array .= ", ";
            $array .= "'{$p->getName()}' => \${$p->getName()}";
        }
        $array .= ")";

        return "return \$this->{$receiverFieldName}->handleCall('{$md->getName()}', $array);";
    }
    
    private function getParameters(ReflectionMethod $m)
    {
        $parameters = array();
        
        /* @var $p ReflectionParameter */
        foreach ($m->getParameters() as $p)
        {
            $pd = new Parameter($p->getName());
            $typeHint = $p->getClass();
            if ($typeHint !== null)
                $pd->setTypeHint($typeHint->getName());

            if ($p->isDefaultValueAvailable())
                $pd->setDefaultValue($p->getDefaultValue());
            
            $parameters[] = $pd;
        }
        
        return $parameters;
    }
    
}
