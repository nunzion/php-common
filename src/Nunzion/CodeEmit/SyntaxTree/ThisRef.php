<?php

namespace Nunzion\CodeEmit\SyntaxTree;

class ThisRef extends VariableRef {

    public function __construct()
    {
        parent::__construct("this");
    }
} 