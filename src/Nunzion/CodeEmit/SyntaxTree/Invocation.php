<?php


namespace Nunzion\CodeEmit\SyntaxTree;


class Invocation implements Expression, Statement {

    private $args;

    public function getArguments()
    {
        return $this->args;
    }

    public function setArguments(array $args)
    {
        $this->args = $args;
    }

    public function addArgument(Expression $argument)
    {
        $this->args[] = $argument;
    }
} 