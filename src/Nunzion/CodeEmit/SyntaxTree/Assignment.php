<?php


namespace Nunzion\CodeEmit\SyntaxTree;

class Assignment extends Node implements Expression, Statement {

    public function __construct(Expression $target, Expression $value)
    {
        $this->target = $target;
        $this->value = $value;
    }

    /**
     * @var Expression
     */
    private $value;

    /**
     * @var Expression
     */
    private $target;

    /**
     * @return Expression
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param Expression $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return Expression
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param Expression $target
     */
    public function setTarget($target)
    {
        $this->target = $target;
    }


    public function generateCode(EmitContext $context)
    {
        return $this->target->generateCode();
    }
}