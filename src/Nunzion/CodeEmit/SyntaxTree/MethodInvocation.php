<?php

namespace Nunzion\CodeEmit\SyntaxTree;

class MethodInvocation extends Invocation {

    private $target;
    private $methodName;

    public function setTarget(Expression $target)
    {
        $this->target = $target;
    }

    public function getTarget()
    {
        return $this->target;
    }

    public function setMethodName($methodName)
    {
        $this->methodName = $methodName;
    }

    public function getMethodName()
    {
        return $this->methodName;
    }
}