<?php

namespace Nunzion\CodeEmit\SyntaxTree;

class MemberVisibility
{
    private static $public;
    
    public static function getPublic()
    {
        if (self::$public === null)
            self::$public = new MemberVisibility("public");
        return self::$public;
    }
    
    private static $protected;
    
    public static function getProtected()
    {
        if (self::$protected === null)
            self::$protected = new MemberVisibility("protected");
        return self::$protected;
    }
    
    private static $private;
    
    public static function getPrivate()
    {
        if (self::$private === null)
            self::$private = new MemberVisibility("private");
        return self::$private;
    }

    
    /**
     * @var string
     */
    private $visibility;
    
    private function __construct($visibility)
    {
        $this->visibility = $visibility;
    }
    
    /**
     * @return string
     */
    public function getVisibility()
    {
        return $this->visibility;
    }
    
    public function __toString()
    {
        return $this->visibility;
    }
}