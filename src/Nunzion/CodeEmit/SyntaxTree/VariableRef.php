<?php

namespace Nunzion\CodeEmit\SyntaxTree;

class VariableRef implements Expression  {

    private $name;

    public function __construct($name = null)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
} 