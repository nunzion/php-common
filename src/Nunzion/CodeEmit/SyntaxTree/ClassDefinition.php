<?php

namespace Nunzion\CodeEmit\SyntaxTree;

class ClassDefinition
{
    /**
     * @var string
     */
    private $fullName;
    
    /**
     * @var Method[]
     */
    private $methods;
    
    
    /**
     *
     * @var string
     */
    private $baseClass;
    
    /**
     *
     * @var string[]
     */
    private $implementedInterfaces;
    
    /**
     * 
     * @param string $fullName
     */
    public function __construct($fullName = null)
    {
        \Nunzion\Expect::that($fullName)->isNullOrString();
        
        $this->fullName = $fullName;
        $this->methods = array();
        $this->implementedInterfaces = array();
    }
    
    /**
     * 
     * @return string|null
     */
    public function getBaseClass()
    {
        return $this->baseClass;
    }
    
    /**
     * 
     * @param string|null $fullClassName
     */
    public function setBaseClass($fullClassName)
    {
        \Nunzion\Expect::that($fullClassName)->isNullOrString();
        
        $this->baseClass = $fullClassName;
    }
    
    /**
     * @param string[] $implementedInterfaces
     */
    public function setImplementedInterfaces(array $implementedInterfaces)
    {
        $this->implementedInterfaces = $implementedInterfaces;
    }
    
    /**
     * @return string[]
     */
    public function getImplementedInterfaces()
    {
        return $this->implementedInterfaces;
    }
    
    public function addImplementedInterface($fullInterfaceName)
    {
        $this->implementedInterfaces[] = $fullInterfaceName;
    }
    
    
    
    /**
     * 
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }
    
    /**
     * 
     * @param string $name
     */
    public function setFullName($name)
    {
        \Nunzion\Expect::that($name)->isString();
        
        $this->fullName = $name;
    }
    
    /**
     * @param Method[] $methods
     */
    public function setMethods(array $methods)
    {
        $this->methods = $methods;
    }
    
    /**
     * @return Method[]
     */
    public function getMethods()
    {
        return $this->methods;
    }
    
    public function addMethod(Method $method)
    {
        $this->methods[] = $method;
    }
}