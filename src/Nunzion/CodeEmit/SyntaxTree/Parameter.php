<?php

namespace Nunzion\CodeEmit\SyntaxTree;

class Parameter
{
    private static $noDefaultValue;
    
    public static function NoDefaultValue()
    {
        if (self::$noDefaultValue === null)
            self::$noDefaultValue = (object)array("value" => "noDefaultValue");
        return self::$noDefaultValue;
    }
    
    /**
     * @var string
     */
    private $name;
    
    /**
     * @var string
     */
    private $typeHint;
    
    private $defaultValue;
    
    public function __construct($name = null, $typeHint = null)
    {
        if ($name !== null)
            $this->setName($name);
        if ($typeHint !== null)
            $this->setTypeHint($typeHint);
        
        $this->setDefaultValue(self::NoDefaultValue());
    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * 
     * @param string $name
     */
    public function setName($name)
    {
        \Nunzion\Expect::that($name)->isString();
        
        $this->name = $name;
    }
    
    /**
     * Sets the type hint.
     * Can be null to disable type hinting.
     * The type name must be the full name of the type and can reference a class or interface.
     * 
     * @param string $typeName
     */
    public function setTypeHint($typeName)
    {
        \Nunzion\Expect::that($typeName)->isNullOrString();
        
        $this->typeHint = $typeName;
    }
    
    /**
     * 
     * @return string
     */
    public function getTypeHint()
    {
        return $this->typeHint;
    }
    
    /**
     * 
     * @return string
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }
        
    /**
     * 
     * @param string $defaultValue
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
    }

    /**
     * 
     * @return bool
     */
    public function hasDefaultValue()
    {
        return $this->defaultValue !== self::NoDefaultValue();
    }
}