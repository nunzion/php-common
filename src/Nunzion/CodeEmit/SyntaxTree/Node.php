<?php


namespace Nunzion\CodeEmit\SyntaxTree;


abstract class Node {
    public abstract function generateCode(EmitContext $context);
} 