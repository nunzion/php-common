<?php

namespace Nunzion\CodeEmit\SyntaxTree;

class Method
{
    /**
     *
     * @var Parameter[]
     */
    private $parameters;
    
    /**
     *
     * @var MemberVisibility
     */
    private $visibility;
    
    /**
     *
     * @var string
     */
    private $name;
    
    /**
     *
     * @var string
     */
    private $code;
    
    /**
     * 
     * @param string $name
     */
    public function __construct($name = null)
    {
        \Nunzion\Expect::that($name)->isNullOrString();
        
        $this->name = $name;
        $this->parameters = array();
        $this->visibility = MemberVisibility::getPublic();
    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * 
     * @param string $name
     */
    public function setName($name)
    {
        \Nunzion\Expect::that($name)->isString();
        
        $this->name = $name;
    }
    
    /**
     * @return Parameter[]
     */
    public function getParameters()
    {
        return $this->parameters;
    }
    
    /**
     * 
     * @param Parameter[] $parameters
     */
    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;
    }
        
    public function addParameter(Parameter $parameter)
    {
        $this->parameters[] = $parameter;
    }
    
    public function setVisibility(MemberVisibility $visibility)
    {
        $this->visibility = $visibility;
    }
    
    public function getVisibility()
    {
        return $this->visibility;
    }
    
    /**
     * Sets the code of the method.
     * Pass null to make this method abstract.
     * 
     * @param string|null $code
     */
    public function setCode($code)
    {
        \Nunzion\Expect::that($code)->isNullOrString();
        $this->code = $code;
    }
    
    /**
     * 
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * @return bool
     */
    public function isAbstract()
    {
        return $this->code === null;
    }
}