<?php

namespace Nunzion\CodeEmit\SyntaxTree;


class Constructor extends Method
{
    function __construct()
    {
        parent::__construct("__construct");
    }
} 