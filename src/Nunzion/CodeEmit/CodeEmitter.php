<?php

namespace Nunzion\CodeEmit;

use Nunzion\StringHelper;
use Nunzion\CodeEmit\SyntaxTree\ClassDefinition;

class CodeEmitter
{
    public function emit(ClassDefinition $class)
    {
        $result = "";
        
        $info = $this->getTypeNameInfo($class->getFullName());
        
        $result .= "namespace {$info->ns};\n";
        
        $result .= "class {$info->name}";
        
        if ($class->getBaseClass() !== null)
            $result .= " extends \\" . $class->getBaseClass();
        
        $first = true;
        foreach ($class->getImplementedInterfaces() as $intf)
        {
            if ($first)
            {
                $first = false;
                $result .= " implements ";
            }
            else
                $result .= ", ";
            
            $result .= "\\" . $intf;
        }
        
        
        $result .= "\n{\n";
        
        foreach ($class->getMethods() as $m)
        {
            $result .= "{$m->getVisibility()} function {$m->getName()}(";
            
            $first = true;
            foreach ($m->getParameters() as $p)
            {
                if ($first)
                    $first = false;
                else
                    $result .= ", ";
                
                if ($p->getTypeHint() !== null)
                    $result .= "\\" . $p->getTypeHint() . " ";
                $result .= '$';
                $result .= $p->getName();
                if ($p->hasDefaultValue())
                    $result .= " = " . var_export($p->getDefaultValue(), true);
            }
            
            $result .= ")";
            
            if ($m->isAbstract())
                $result .= ";\n";
            else 
                $result .= "\n{\n{$m->getCode()}\n}\n";
        }
        
        $result .= "}\n";
        
        return $result;
    }
    
    private function getTypeNameInfo($fullTypeName)
    {
        $pos = strrpos($fullTypeName, "\\");
        
        $ns = substr($fullTypeName, 0, $pos);
        $name = substr($fullTypeName, $pos + 1);
        
        $ns = StringHelper::removeStart($ns, "\\");
        
        return (object)array("ns" => $ns, "name" => $name);
    }
}
