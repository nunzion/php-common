<?php

namespace Nunzion\CodeEmit;

class MockupCallLogger implements MockupCallReceiver
{
    /**
     * @var LoggedMockupCall
     */
    private $calls = array();
    
    public function handleCall($methodName, array $args)
    {
        $this->calls[] = new LoggedMockupCall($methodName, $args);
    }
    
    /**
     * @return LoggedMockupCall[]
     */
    public function getCalls()
    {
        return $this->calls;
    }
}