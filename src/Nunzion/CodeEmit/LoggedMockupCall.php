<?php

namespace Nunzion\CodeEmit;

class LoggedMockupCall
{

    /**
     * @var array
     */
    private $arguments;
    
    /**
     *
     * @var string
     */
    private $methodName;

    /**
     * @return array
     */
    public function getArguments()
    {
        return $this->arguments;
    }
    
    /**
     * @return string
     */
    public function getMethodName()
    {
        return $this->methodName;
    }
    
    /**
     * 
     * @param string $methodName
     * @param array $arguments
     */
    public function __construct($methodName, array $arguments)
    {
        $this->methodName = $methodName;
        $this->arguments = $arguments;
    }
}