<?php

namespace Nunzion\CodeEmit;

interface MockupCallReceiver
{
    /**
     * @param string $methodName
     * @param array $args
     */
    function handleCall($methodName, array $args);
}
