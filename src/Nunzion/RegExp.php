<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion;

class RegExp
{
    private $pattern;
    
    public function __construct($pattern)
    {
        $this->pattern = $pattern;
    }
    
    public function __invoke($subject)
    {
        return preg_match($this->pattern, $subject);
    }
    
    public function getHashCode()
    {
        return "RegExp:" . $this->pattern;
    }
    
    public function toString()
    {
        return "Regular Expression (" . $this->pattern . ")";
    }
}
