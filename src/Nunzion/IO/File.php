<?php

namespace Nunzion\IO;

use Nunzion\Expect;

class File
{
    /**
     * 
     * @param string $path
     * @return File
     */
    public static function makeAbsolute($path, Directory $baseDir)
    {
        if (preg_match("/^[a-zA-Z]:\\\\/", $path))
            return new File($path);
        
        return new File($baseDir->getPath() . "/" . $path);
    }
    
    private $exists;
    private $path;
    
    /**
     * 
     * @param string $path
     */
    public function __construct($path)
    {
        Expect::that($path)->isString()->isNotEmpty();
        $realPath = realpath($path);
        if ($realPath === false)
        {
            $realPath = realpath(dirname($path));
            if ($realPath === null)
                Expect::that($path)->_("must be a valid file path, but was '{actual}'", 
                    array("actual" => $path));
            $realPath .= "/" . basename($path);
        }
        $this->path = $realPath;
    }
    
    public function getPath()
    {
        return $this->path;
    }
    
    /**
     * 
     * @return Directory
     */
    public function getDirectory()
    {
        return new Directory(dirname($this->path));
    }
    
    /**
     * Gets the content of the file.
     * 
     * @return string the content of the file.
     */
    public function getContent()
    {
        return file_get_contents($this->path);
    }
}