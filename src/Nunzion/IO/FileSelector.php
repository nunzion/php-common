<?php

namespace Nunzion\IO;

use Iterator;

interface FileSelector
{
    /**
     * Gets a list of files.
     *
     * @return Iterator<File> the files.
     */
    function getFiles();
}