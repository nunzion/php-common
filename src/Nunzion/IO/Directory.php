<?php

namespace Nunzion\IO;

use Nunzion\Expect;

class Directory
{
    /**
     * 
     * @param string $path
     * @return Directory
     */
    public static function makeAbsolute($path, Directory $baseDir)
    {
        if (preg_match("/^[a-zA-Z]:\\\\/", $path))
            return new Directory($path);
        
        return new Directory($baseDir->getPath() . "/" . $path);
    }
    
    public static function createIfNotExist($path)
    {
        if (mkdir($path, 0777, true))
            return new Directory($path);
        else
            return null;
    }
    
    /**
     * @var string
     */
    private $path;

    /**
     * Creates a directory object.
     * 
     * @param string $path the path of the directory. 
     * Can be relative or absolute and the directory must exist.
     */
    public function __construct($path)
    {
        Expect::that($path)->isString()->isNotEmpty();
        $realPath = realpath($path);
        if ($realPath === false)
            Expect::that($path)->_("must be a valid file path, but was '{actual}'", 
                    array("actual" => $path));

        $this->path = $realPath;
    }

    public function getFile($relativePath)
    {
        return new File($this->path . "/" . $relativePath);
    }
    
    public function getSubDirectory($relativePath)
    {
        return new Directory($this->path . "/" . $relativePath);
    }
    
    /**
     * Gets the absolute path to the directory.
     * 
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $filter
     * @return Iterator<File>
     */
    public function enumerateFiles($filter, $recursive = false)
    {
        $result = array();
        foreach ($this->rglob($this->path . "/" . $filter) as $file)
        {
            $result[] = new File($file);
        }
        return new \ArrayIterator($result);
    }

    private function rglob($pattern, $flags = 0)
    {
        $files = glob($pattern, $flags);
        foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir)
        {
            $files = array_merge($files,
                    $this->rglob($dir . '/' . basename($pattern), $flags));
        }
        return $files;
    }

    public function __toString()
    {
        return $this->path;
    }
}
