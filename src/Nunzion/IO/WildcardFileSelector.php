<?php

namespace Nunzion\IO;

use Iterator;
use Nunzion\Expect;

class WildcardFileSelector implements FileSelector
{

    private $recursive;
    private $filter;
    private $directory;
    
    /**
     * @param \Nunzion\IO\Directory $directory (default attribute)
     * @param string $filter (default attribute)
     * @param boolean $recursive
     */
    public function __construct(Directory $directory, $filter, $recursive = true)
    {
        Expect::that($filter)->isString();
        
        $this->directory = $directory;
        $this->filter = $filter;
        $this->recursive = $recursive;
    }
    
    /**
     * Gets a list of files.
     *
     * @return Iterator<string> the files.
     */
    function getFiles()
    {
        return $this->directory->enumerateFiles($this->filter, $this->recursive);
    }
}