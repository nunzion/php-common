<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion;

class Select
{
    public static function member($memberName)
    {
        return function ($input) use ($memberName) {
            return $input->$memberName;
        };
    }
    
    public static function join($selector1, $selector2)
    {
        $selectors = func_get_args();
        return function ($input) use ($selectors) {
            $currentInput = $input;
            foreach ($selectors as $selector)
                $currentInput = $selector($currentInput);
            return $currentInput;
        };
    }
    
    //$o->Foo->Bar
    //Select::join(Select::member("Foo"), Select::member("Bar"))
}
