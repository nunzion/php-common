<?php

namespace Nunzion;

use Nunzion\NotImplementedException;

class InvokeAction
{
    /**
     * @var string
     */
    private $methodName;
    /**
     * @var PhpType
     */
    private $type;
    /**
     * @var object
     */
    private $instance;
    /**
     * @var array
     */
    private $arguments;

    /**
     * @param string $methodName
     * @param PhpType $type
     * @param object $instance
     * @param array $arguments
     */
    public function __construct($methodName, $type = null, $instance = null, array $arguments = array())
    {
        $this->methodName = $methodName;
        $this->type = $type;
        $this->instance = $instance;
        $this->arguments = $arguments;
    }

    public function __invoke()
    {
        $instance = $this->instance;
        $method = $this->methodName;
        throw new NotImplementedException();
    }
} 