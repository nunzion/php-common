<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion;

class StringHelper
{
    public static function startsWith($subject, $start, $caseSensitive = true)
    {
        return $caseSensitive ?
            (strcmp(substr($subject, 0, strlen($start)), $start) === 0) :
            (strcasecmp(substr($subject, 0, strlen($start)), $start) === 0);
    }


    public static function endsWith($subject, $end, $caseSensitive = true)
    {
        return $caseSensitive ?
            (strcmp(substr($subject, -strlen($end)), $end) === 0) :
            (strcasecmp(substr($subject, -strlen($end)), $end) === 0);
    }
    
    public static function removeEnd($subject, $end, $caseSensitive = true)
    {
        if (self::endsWith($subject, $end, $caseSensitive))
        {
            $r = substr($subject, 0, -strlen($end));
            if ($r === false)
                return "";
            return $r;
        }
        else
            return $subject;
    }

    public static function removeStart($subject, $start, $caseSensitive = true)
    {
        if (self::startsWith($subject, $start, $caseSensitive))
        {
            $r = substr($subject, strlen($start));
            if ($r === false)
                return "";
            return $r;
        }
        else
            return $subject;
    }

    
    public static function contains($subject, $text)
    {
        return strpos($subject, $text) !== false;
    }

    
    public static function format($template, $arguments)
    {
        //TODO: Implement escaping!
        //{foo}test{foo:bar parameter}
        
        $arguments = func_get_args();
        
        array_shift($arguments);
        
        if (count($arguments) === 1 && is_array($arguments[0]))
            $arguments = $arguments[0];
        
        $result = preg_replace_callback('/\\{(?<parameterName>.*?)(\\:(?<formatOptions>.*))?\\}/', 
            function ($match) use ($arguments) {
                $parameterName = $match["parameterName"];
                if (isset($arguments[$parameterName]))
                    return $arguments[$parameterName];
            }, $template);
            
        return $result;
    }
    
    public static function capitalize($str)
    {
        return strtoupper(substr($str, 0, 1)) . substr($str, 1);
    }
    
    public static function uncapitalize($str)
    {
        return strtolower(substr($str, 0, 1)) . substr($str, 1);
    }
}
