<?php

namespace Nunzion\Types;

class ReflectionClass extends \ReflectionClass
{
    /**
     * 
     * @return ReflectionMethod|null
     */
    public function getConstructor()
    {
        $result = parent::getConstructor();
        if ($result === null)
            return null;
        return new ReflectionMethod($result->class, $result->name);
    }
    
    /**
     * 
     * @param string $name
     * @return ReflectionMethod
     */
    public function getMethod($name)
    {
        $result = parent::getMethod($name);
        return new ReflectionMethod($result->class, $result->name);
    }
    
    public function getMethods($filter = null)
    {
        if ($filter == null)
            $methods = parent::getMethods();
        else
            $methods = parent::getMethods($filter);
        
        $result = array();
        foreach ($methods as $m)
        {
            $result[] = new ReflectionMethod($m->class, $m->name);
        }
        
        return $result;
    }
}