<?php

namespace Nunzion\Types;

class Method extends ReflectionMethod
{    
    /**
     *
     * @var Type
     */
    private $type;
    
    /**
     * 
     * @param Type $type (default attribute)
     * @param string $name (default attribute)
     */
    public function __construct($type, $name)
    {
        parent::__construct($type->getName(), $name);
        
        $this->type = $type;
    }
    
    public function getType()
    {
        return $this->type;
    }    
}
