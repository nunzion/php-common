<?php

namespace Nunzion\Types;

use Exception;
use Nunzion\Expect;
use Nunzion\StringHelper;

class Type
{
    public static function _getClassName()
    {
        return get_called_class();
    }
    
    
    public static function ofBool()
    {
        return self::of(self::BoolName);
    }

    public static function ofObject()
    {
        return self::of(self::ObjectName);
    }
    
    public static function ofString()
    {
        return self::of(self::StringName);
    }
    
    public static function ofInt()
    {
        return self::of(self::IntName);
    }
    
    public static function ofDouble()
    {
        return self::of(self::DoubleName);
    }
    
    
    
    
    
    
    private static $cachedTypes;
    
    /**
     * 
     * @param string $fullTypeName
     * @return Type
     */
    public static function of($fullTypeName)
    {
        $fullTypeName = StringHelper::removeStart($fullTypeName, "\\");
        
        if (!isset(self::$cachedTypes[$fullTypeName]))
            self::$cachedTypes[$fullTypeName] = Type::internalCreate($fullTypeName);
        
        return self::$cachedTypes[$fullTypeName];
    }

     
    
    const BoolName = "bool";
    const BooleanName = "boolean";
    const IntName = "int";
    const IntegerName = "integer";
    const ArrayName = "array";
    const ObjectName = "object";
    const StringName = "string";
    const DoubleName = "double";
    const RealName = "real";
    const FloatName = "float";
    const CallableName = "callable";
    
    public static function internalCreate($fullTypeName)
    {
        $genericTypeArguments = array();
        if (StringHelper::endsWith($fullTypeName, "[]"))
        {
            $typeName = StringHelper::removeEnd($fullTypeName, "[]");
            $genericTypeArguments[] = self::of($typeName);
            $fullTypeName = "array";
        }
        
        return new Type($fullTypeName, $genericTypeArguments);
    }
    
    
    
    
    
    
    const BaseType = 0;
    const ClassType = 1;
    const InterfaceType = 2;
    
    

    
    private $type;
    private $typeTest;


    
    
    /**
     * @var Type[]
     */
    private $genericTypeArguments;
    

    private function __construct($fullTypeName, array $genericTypeArguments)
    {
        //interface, class, base type
        //MyClass<int,bool>
        //string[] => array<string>
        //string[string] => array<string, string>
        
        if (strtolower($fullTypeName) === self::RealName || strtolower($fullTypeName) == self::FloatName)
            $fullTypeName = self::DoubleName;
        
        if (strtolower($fullTypeName) === self::IntegerName)
            $fullTypeName = self::IntName;
        
        if (strtolower($fullTypeName) === self::BooleanName)
            $fullTypeName = self::BoolName;
        
        $baseTypes = array(
            self::BoolName => "is_bool",
            self::IntName => "is_int",
            self::ArrayName => "is_array",
            self::ObjectName => function ($other) { return true; },
            self::StringName => "is_string",
            self::DoubleName => "is_double",
            self::CallableName => "is_callable");

        if (array_key_exists(strtolower($fullTypeName), $baseTypes))
        {
            $this->type = self::BaseType;
            $this->typeTest = $baseTypes[strtolower($fullTypeName)];
        }
        else if (class_exists($fullTypeName) | interface_exists($fullTypeName))
        {
            if (class_exists($fullTypeName))
                $this->type = self::ClassType;
            else if (interface_exists($fullTypeName))
                $this->type = self::InterfaceType;
            
            $this->typeTest = function($t) use ($fullTypeName) {
                return $t instanceof $fullTypeName;
            };
        }
        else
            Expect::that($fullTypeName)->_(
                    "must be a valid type name, but '{actual}' is not a valid type name", 
                    array("actual" => $fullTypeName));

        $this->typeName = $fullTypeName;
        $this->genericTypeArguments = $genericTypeArguments;
    }

    
    /**
     * @var string
     */
    private $typeName;
    
    public function getName()
    {
        return $this->typeName;
    }    
    
    
    /**
     * @var ReflectionClass
     */
    private $reflectionClass;
    
    /**
     * @return ReflectionClass
     */
    public function getReflectionClass()
    {    
        if ($this->reflectionClass == null)
        {
            if (!$this->isClass() && !$this->isInterface())
                throw new Exception("Can reflect only classes or interfaces.");
            $this->reflectionClass = new ReflectionClass($this->typeName);
        }   
        
        return $this->reflectionClass;
    }
    
    public function isArray()
    {
        return $this->typeName === "array";
    }
    
    /**
     * @return bool
     */
    public function isClass()
    {
        return $this->type == self::ClassType;
    }
    
    /**
     * @return bool
     */
    public function isInterface()
    {
        return $this->type == self::InterfaceType;
    }
    
    /**
     * @return bool
     */
    public function isPrimitive()
    {
        return $this->type == self::BaseType;
    }
    
    /**
     * @return bool
     */
    public function isGenericParameter()
    {
        
    }
    
    
    /**
     * Even primitive types can be generic (e.g. array)
     * 
     * @return Type[]
     */
    public function getGenericTypeArguments()
    {
        return $this->genericTypeArguments;
    }
    
    private $genericTypeDefinition;
    
    /**
     * @return Type
     */
    public function getGenericTypeDefinition()
    {
        
    }
    
    
    
    
    
    
    
    /**
     * 
     * @param string $methodName
     * @return ReflectionMethod
     */
    public function getMethod($methodName)
    {
        return $this->getReflectionClass()->getMethod($methodName);
    }
    
    
    public function newInstance()
    {
        
    }
    
    
    
    public function isTypeOf($object)
    {
        if ($object === null)
            return true;
        $f = $this->typeTest;
        return $f($object);
    }
    
   
    
    public function __toString()
    {
        return $this->typeName;
    }
}