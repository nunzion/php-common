<?php

namespace Nunzion\Types;

class ReflectionMethod extends \ReflectionMethod
{    
    public function getParameters()
    {
        $result = array();
        $parameters = parent::getParameters();
        /* @var $p \ReflectionMethod */
        foreach ($parameters as $p)
        {
            $result[] = new ReflectionParameter(
                    array($this->getDeclaringClass()->getName(), 
                        $this->getName()), $p->getName());
        }
        
        return $result;
    }
}