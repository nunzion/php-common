<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion;

class ArrayHelper
{
    public static function tryGetValue(array $array, $value, $default = null)
    {
        if (array_key_exists($value, $array))
            return $array[$value];
        else
            return $default;
    }
    
    public static function tryGetNotNullValue(array $array, $value, $default)
    {
        if (isset($array[$value]))
            return $array[$value];
        else
            return $default;
    }
    
    public static function any(array $array, $predicate)
    {
        Expect::that($predicate)->isCallable();
        
        foreach ($array as $element)
            if ($predicate($element))
                return true;
        return false;
    }
    
    public static function all(array $array, $predicate)
    {
        Expect::that($predicate)->isCallable();
        
        foreach ($array as $element)
            if (!$predicate($element))
                return false;
        return true;
    }
    
    /**
     * Transforms each element to a string.
     * 
     * @param array $array the array.
     * @return string[] the result array.
     */
    public static function itemsToString(array $array)
    {
       $result = array();
       foreach ($array as $item)
           $result[] = (string)$item;
       return $result;
    }
    
    /**
     * Transforms an array to a string by transforming each element to a string
     * and concatenizing them.
     * @param array $array
     * @return string
     */
    public static function toString(array $array, $separator)
    {
       $result = array();
       foreach ($array as $item)
           $result[] = (string)$item;
       return implode($separator, $result);
    }
    
    public static function firstOfType(array $array, $className)
    {
        $result = null;
        foreach ($array as $a)
            if (is_a($a, $className))
                $result = $a;
        return $result;
    }
}
